import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SignupComponent } from './signup/signup.component';
import { ComponentsModule } from 'app/shared/components/components.module';
import { RegisterComponent } from 'app/shared/components/sessions/register/register.component';
import { LoginComponent } from './login/login.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        NgbModule,
        ComponentsModule
        
    ],
    declarations: [
        SignupComponent,
        LoginComponent
        
    ],
    
})
export class SessionsModule { }
