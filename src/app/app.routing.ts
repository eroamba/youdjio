import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { ComponentsComponent } from './shared/components/components.component';
import { NucleoiconsComponent } from './shared/components/nucleoicons/nucleoicons.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SignupComponent } from './sessions/signup/signup.component';
import { LandingComponent } from './pages/landing/landing.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './sessions/login/login.component';
import { ProductComponent } from './pages/product/product.component';
import { HairComponent } from './pages/hair/hair.component';
import { TipComponent } from './pages/tip/tip.component';
import { BlogComponent } from './pages/blog/blog.component';
import { ContactComponent } from './pages/contact/contact.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
/*     { path: 'home',             component: ComponentsComponent },
 */    { path: 'home', component: HomeComponent },
  { path: 'user-profile', component: ProfileComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'nucleoicons', component: NucleoiconsComponent },

    { path: 'product', component: ProductComponent },
  { path: 'hair', component: HairComponent },
  { path: 'tip', component: TipComponent },
  { path: 'blog', component: BlogComponent },
  { path: 'contact', component: ContactComponent },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
