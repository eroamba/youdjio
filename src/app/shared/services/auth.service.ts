import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';


import { ApiService } from './base/api.service';
import { BaseRepository } from './base/baseRepository';
import { User } from '../models';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseRepository<User> {
  jwtHelper = new JwtHelperService();
  decodedToken: any;
  currentUser: User;
  photoUrl = new BehaviorSubject<string>('../../../assets/img/user.png');
  currentPhotoUrl = this.photoUrl.asObservable();

  constructor(apiService: ApiService) {
    super('auth', apiService);
  }

  changeMemberPhoto(photoUrl: string) {
    this.photoUrl.next(photoUrl);
  }

  login(model: any) {
    return this.signin(model).pipe(
      map((response: any) => {
        const user = response;
        if (user) {
          localStorage.setItem('token', user.token);
          localStorage.setItem('user', JSON.stringify(user.user));
          this.decodedToken = this.jwtHelper.decodeToken(user.token);
          this.currentUser = user.user;
          /* this.changeMemberPhoto(this.currentUser.photoUrl); */
        }
      })
    );
  }

  register(user: User) {
    return this.signup(user);
  }

  loggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
}
