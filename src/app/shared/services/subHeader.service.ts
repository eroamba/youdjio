import { Injectable } from "@angular/core";
import { SubHeader } from "../models/subHeader";

@Injectable()

export class SubHeaderService {

    data: SubHeader[];

    /**
     *
     */
    constructor() {

    }

    getAll() {
        return this.data = [{
            title: 'Choisissez',
            subTitle: "votre ville",
            icon: "nc-pin-3"
        },
        {
            title: 'Contactez',
            subTitle: "nous 7j/7",
            icon: "nc-watch-time"
        },
        {
            title: 'Optez',
            subTitle: "pour une coiffure",
            icon: "nc-scissors"
        }
        ]
    }
}
