import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';



@Injectable()
export class ApiService {

constructor(private http: HttpClient,
  ) { }

  getUrl(path: string): string {

    if (path.indexOf('http') >= 0) {
      return path;
    }

    const url = `${environment.apiUrl}${path}`;
    return url;
  }


  get(path: string, httpOptions?: Object): Observable<any> {
    return this.http.get(this.getUrl(path), httpOptions)
  }

  post(path: string, body: any): Observable<any> {
    return this.http.post(this.getUrl(path), body)
  }



/* 
  handleError(response: HttpErrorResponse) {

    const error = response.error;
    let errorMessage = '';

    if (typeof error === 'string' || error instanceof String) {
      errorMessage = response.error;
    } else if (error && typeof error === 'object' && error.constructor === Object) {
      errorMessage = response.error.message;
    }
    switch (response.status) {
      case 400:
        return throwError(new BadInpunt(errorMessage, response));
      case 401:
        return throwError(new NotAuthorize('Vous n\'êtes pas autorisé à accéder à cette ressource', response));
      case 404:
        return throwError(new NotFoundError(errorMessage, response));
      case 500:
        return throwError(new InternalServerError('Une erreur non gérée s\'est produise dans l\'application. ' +
          'Veuillez réessayer ou contactez l\'administrateur.', response));
      case 0:
        return throwError(new NetworkError('Erreur de connection vérifiez que vous est connecté au réseau.', response));
      default:
        return throwError(new AppError(errorMessage, response));
    }
  } */




}
