import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

import { ModelBase } from '../../models/base';

export class BaseRepository<T extends ModelBase > {

constructor(protected controllerName: string,
  public apiService: ApiService) { }


  signin(model: any){
    return this.apiService.post(`${this.controllerName}/login`, model)
  }

  signup(model: T) {
    return this.apiService.post(`${this.controllerName}/register`, model);
  }

  getAll(): Observable<T[]> {
    return this.apiService.get(`${this.controllerName}/getAll`);
  }

  getById(id: string): Observable<T> {
      return this.apiService.get(`${this.controllerName}/getById/${id}`);
  }

  save(model: T): Observable<T> {
    if (!model.id) {
        return this.apiService.post(`${this.controllerName}/add`, model);
    } else {
/*         return  this.apiService.put(`/${this.controllerName}/update/${model.id}`, model)
 */    }
}


}
