import { ModelBase } from "./modelBase";

export interface KeyValuePair extends ModelBase {
    name:string;
}
