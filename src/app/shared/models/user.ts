import { ModelBase } from "./base";
import { Gender, MaritalStatus, UserRole, UserSocialNetWork,
         UserSkill, UserExperience, UserLanguage, UserSchoolLevel, UserActualJobStatus, UserJobSectorAsk } from ".";



export interface User extends ModelBase {
    username: string;
    firstName: string;
    lastName: string;
    genderId: number;
    MaritalStatusId: number;
    phoneNumber: string;
    dateOfBirth: Date;
    city: string;
    country: string;
    driverLicence: Boolean;
    passport: Boolean;
    photoPath: string;
    created: Date;
    lastActive: Date;
    gender: Gender;
    maritalStatus: MaritalStatus;
    roles: UserRole;
    socialNetWorks: UserSocialNetWork;
    skills: UserSkill;
    experiences: UserExperience;
    languages: UserLanguage;
    schoolLevels: UserSchoolLevel;
    actualJobStatuses: UserActualJobStatus;
    jobSectorAsks: UserJobSectorAsk;






}
