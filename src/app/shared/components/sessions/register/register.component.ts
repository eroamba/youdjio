import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormControlName } from '@angular/forms';

import { User } from 'app/shared/models';
import { AuthService } from 'app/shared/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  user: User;

  firstNameFormControl: FormControl;
  lastNameFormControl: FormControl;
  emailFormControl: FormControl;
  phoneNumberFormControl: FormControl;
  passwordFormControl: FormControl;

  constructor(private fb: FormBuilder,
    private authService: AuthService,
    private toastrService: ToastrService,
    private router: Router) {
    this.createForm();

    this.createFormControls();
  }

  ngOnInit() {
  }

  createForm() {
    this.registerForm = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.required],
      phoneNumber: ['', [Validators.required, Validators.pattern('[0-9]+'),
      Validators.minLength(8), Validators.maxLength(8)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    })
  }

  createFormControls() {
    this.firstNameFormControl = this.registerForm.get('firstName') as FormControl;
    this.lastNameFormControl = this.registerForm.get('lastName') as FormControl;
    this.emailFormControl = this.registerForm.get('email') as FormControl;
    this.passwordFormControl = this.registerForm.get('password') as FormControl;
    this.phoneNumberFormControl = this.registerForm.get('phoneNumber') as FormControl;
  }

  register() {
    
    if (this.registerForm.valid) {
      this.user = Object.assign({}, this.registerForm.value);
      this.authService.register(this.user).subscribe(
        () => {
          this.toastrService.success('Registration succesful');
        },
        error => {
          this.toastrService.error(error);
        },
        () => {
          this.authService.login(this.user).subscribe(() => {
            this.router.navigate(['/login']);
          });
        }
      );
    }
  }

}
