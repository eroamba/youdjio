import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from 'app/shared/services/auth.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.scss']
})
export class ConnexionComponent implements OnInit {
  connexionForm: FormGroup;

  loginFormControl: FormControl;
  passwordFormControl: FormControl;

  constructor(public authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService) {
    this.createForm();

    this.createFormControls();
  }

  ngOnInit() {
  }

  createForm() {
    this.connexionForm = this.fb.group({
      login: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  createFormControls() {
    this.loginFormControl = this.connexionForm.get('login') as FormControl;
    this.passwordFormControl = this.connexionForm.get('password') as FormControl;
  }

  login() {
    this.authService.login(this.connexionForm.value).subscribe(
      next => {
        this.toastr.success('Logged in successfully');
      },
      error => {
        this.toastr.error(error);
      },
      () => {
        this.router.navigate(['/landing']);
      }
    );
  }

 

}
