import { Component, OnInit, Input } from '@angular/core';
import { SubHeader } from 'app/shared/models/subHeader';

@Component({
  selector: 'app-sub-header',
  template: `
  <div class="info d-flex justify-content-center">
      <div class="icon icon-neutral mr-2">
          <i class="text-customize-color nc-icon {{value.icon}}"></i>
      </div>
      <div class="description text-dark">
          <h4 class="text-dark">{{value.title}}</h4>
          <h5 class="text-dark">{{value.subTitle}}</h5>
      </div>
  </div>`,
})
export class SubHeaderComponent implements OnInit {

  @Input() value: SubHeader;

  constructor() { }

  ngOnInit() {
  }

}
