import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiService } from './services/base/api.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
})
export class SharedModule { }
