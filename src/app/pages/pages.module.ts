import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { LandingComponent } from './landing/landing.component';
import { ProfileComponent } from './profile/profile.component';
import { BasicelementsComponent } from 'app/shared/components/basicelements/basicelements.component';
import { NavigationComponent } from 'app/shared/components/navigation/navigation.component';
import { TypographyComponent } from 'app/shared/components/typography/typography.component';
import { NucleoiconsComponent } from 'app/shared/components/nucleoicons/nucleoicons.component';
import { NotificationComponent } from 'app/shared/components/notification/notification.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { MatSelectModule } from '@angular/material';


import { NgbdModalComponent, NgbdModalContent } from 'app/shared/components/modal/modal.component';
import { HomeComponent } from './home/home.component';
import { NouisliderModule } from 'ng2-nouislider';
import { RouterModule } from '@angular/router';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { ComponentsModule } from 'app/shared/components/components.module';
import { SubHeaderComponent } from 'app/shared/components/custom-component/sub-header.component';
import { SubHeaderService } from 'app/shared/services/subHeader.service';
import { BlogComponent } from './blog/blog.component';
import { ContactComponent } from './contact/contact.component';
import { HairComponent } from './hair/hair.component';
import { ProductComponent } from './product/product.component';
import { TipComponent } from './tip/tip.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        NouisliderModule,
        RouterModule,
        JwBootstrapSwitchNg2Module,
        ComponentsModule,
        NgSelectModule,
        MatSelectModule

    ],
    declarations: [
        BlogComponent,
        ContactComponent,
        HairComponent,
        ProductComponent,
        LandingComponent,
        ProfileComponent,
        HomeComponent,
        SubHeaderComponent,
        TipComponent
    ],
    entryComponents: [NgbdModalContent,],

})
export class PagesModule { }
