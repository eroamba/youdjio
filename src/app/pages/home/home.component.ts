import { Component, OnInit, Renderer } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder } from '@angular/forms';
import { SubHeaderService } from 'app/shared/services/subHeader.service';
import { SubHeader } from 'app/shared/models/subHeader';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    searchForm: FormGroup;

    subHeaders:SubHeader[];


    constructor(private fb: FormBuilder,private subHeaderService:SubHeaderService) {
        this.createForm();
        this.subHeaders=this.subHeaderService.getAll();
    }

    ngOnInit() {
    }

    createForm() {
        this.searchForm = this.fb.group({
            skillId: null,
            levelId: null,
            schoolId: null,
        })

    }

}
