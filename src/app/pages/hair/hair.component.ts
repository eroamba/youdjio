import { Component, OnInit } from '@angular/core';
import { SubHeaderService } from 'app/shared/services/subHeader.service';
import { SubHeader } from 'app/shared/models/subHeader';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-hair',
  templateUrl: './hair.component.html',
  styleUrls: ['./hair.component.scss']
})
export class HairComponent implements OnInit {


  searchForm:FormGroup;
  selected:string;
  subHeaders: SubHeader[];

  constructor(private subHeaderService: SubHeaderService) {
    this.subHeaders = this.subHeaderService.getAll();
  }

  ngOnInit() {
  }

}
